import time

def time_of_test_excecution(func):
    def take_time(func):
        start_time = time.time()
        func()
        end_time =time.time()
        print("Excecution time: " + str(end_time - start_time))
    return take_time
