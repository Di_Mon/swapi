import pytest
import time

@pytest.fixture(scope="function", autouse=True)
def time_it():
    start = time.time()
    yield
    end = time.time()
    print("\nExecution time is {} seconds".format(end - start))